Drupal.behaviors.conciseCommentsSettings = function(){
  // $("#blueprint_support_options").hide();

  if ($('#edit-blueprint-support').is(':checked')){
    $("#blueprint_support_options").show();
  }
  if ($("#edit-blueprint-support:not('.blueprint-support-processed')").size()) {
    // Add onclick handler to checkbox w/id checkme
    $("#edit-blueprint-support").change(function(){

      // If checked
      if ($("#edit-blueprint-support").is(":checked")) {
        //show the hidden div
        $("#blueprint_support_options").show("fast");
      }
      else {
        //otherwise, hide it 
        $("#blueprint_support_options").hide("fast");
      }
    });
    $("#edit-blueprint-support").addClass('blueprint-support-processed');
  }
}
